# frozen_string_literal: true

require 'lazy_object'

class RegistrationRepo
	class Conflict < StandardError; end

	def initialize(redis: LazyObject.new { REDIS })
		@redis = redis
	end

	def find(jid)
		REDIS.lrange(cred_key(jid), 0, 3)
	end

	def find_jid(tel)
		REDIS.get(jid_key(tel))
	end

	def put(jid, *creds)
		tel = creds.last

		EMPromise.all([
			find(jid),
			REDIS.get(jid_key(tel))
		]).then { |(oldcreds, oldjid)|
			oldtel = oldcreds.last
			next if creds == oldcreds && jid.to_s == oldjid.to_s

			if oldjid && oldjid.to_s != jid.to_s
				raise Conflict, "Another user exists for #{tel}"
			end

			begin
				REDIS.multi
				REDIS.del(jid_key(oldtel)) if oldtel
				REDIS.set(
					jid_key(tel),
					Blather::JID.new(jid).stripped.to_s,
					"NX"
				)
				REDIS.del(cred_key(jid))
				REDIS.rpush(cred_key(jid), *creds)
				REDIS.exec
			rescue StandardError
				REDIS.discard
				raise
			end
		}
	end

	def delete(jid)
		find(jid).then { |creds|
			REDIS.del(
				cred_key(jid),
				jid_key(creds.last)
			)
		}
	end

protected

	def cred_key(jid)
		"catapult_cred-#{Blather::JID.new(jid).stripped}"
	end

	def jid_key(tel)
		"catapult_jid-#{tel}"
	end
end
