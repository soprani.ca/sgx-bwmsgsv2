#!/usr/bin/env ruby
# frozen_string_literal: true

# Copyright (C) 2017-2020  Denver Gingerich <denver@ossguy.com>
# Copyright (C) 2017  Stephen Paul Weber <singpolyma@singpolyma.net>
#
# This file is part of sgx-bwmsgsv2.
#
# sgx-bwmsgsv2 is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# sgx-bwmsgsv2 is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with sgx-bwmsgsv2.  If not, see <http://www.gnu.org/licenses/>.

require 'blather/client/dsl'
require 'em-hiredis'
require 'em-http-request'
require 'json'
require 'multibases'
require 'multihashes'
require 'securerandom'
require "sentry-ruby"
require 'time'
require 'uri'
require 'webrick'

require 'goliath/api'
require 'goliath/server'
require 'log4r'

require 'em_promise'

require_relative 'lib/bandwidth_error'
require_relative 'lib/registration_repo'

Sentry.init

# List of supported MIME types from Bandwidth - https://support.bandwidth.com/hc/en-us/articles/360014128994-What-MMS-file-types-are-supported-
MMS_MIME_TYPES = [
	"application/json",
	"application/ogg",
	"application/pdf",
	"application/rtf",
	"application/zip",
	"application/x-tar",
	"application/xml",
	"application/gzip",
	"application/x-bzip2",
	"application/x-gzip",
	"application/smil",
	"application/javascript",
	"audio/mp4",
	"audio/mpeg",
	"audio/ogg",
	"audio/flac",
	"audio/webm",
	"audio/wav",
	"audio/amr",
	"audio/3gpp",
	"image/bmp",
	"image/gif",
	"image/jpeg",
	"image/pjpeg",
	"image/png",
	"image/svg+xml",
	"image/tiff",
	"image/webp",
	"image/x-icon",
	"text/css",
	"text/csv",
	"text/calendar",
	"text/plain",
	"text/javascript",
	"text/vcard",
	"text/vnd.wap.wml",
	"text/xml",
	"video/avi",
	"video/mp4",
	"video/mpeg",
	"video/ogg",
	"video/quicktime",
	"video/webm",
	"video/x-ms-wmv",
	"video/x-flv"
]

def panic(e)
	Sentry.capture_exception(e)
	puts "Shutting down gateway due to exception: #{e.message}"
	puts e.backtrace
	SGXbwmsgsv2.shutdown
	puts 'Gateway has terminated.'
	EM.stop
end

EM.error_handler(&method(:panic))

def extract_shortcode(dest)
	num, context = dest.split(';', 2)
	num if context == 'phone-context=ca-us.phone-context.soprani.ca'
end

def anonymous_tel?(dest)
	dest.split(';', 2)[1] == 'phone-context=anonymous.phone-context.soprani.ca'
end

class SGXClient < Blather::Client
	def register_handler(type, *guards, &block)
		super(type, *guards) { |*args| wrap_handler(*args, &block) }
	end

	def register_handler_before(type, *guards, &block)
		check_handler(type, guards)
		handler = lambda { |*args| wrap_handler(*args, &block) }

		@handlers[type] ||= []
		@handlers[type].unshift([guards, handler])
	end

protected

	def wrap_handler(*args)
		v = yield(*args)
		v = v.sync if ENV['ENV'] == 'test' && v.is_a?(Promise)
		v.catch(&method(:panic)) if v.is_a?(Promise)
		true # Do not run other handlers unless throw :pass
	rescue Exception => e
		panic(e)
	end
end

# TODO: keep in sync with jmp-acct_bot.rb, and eventually put in common location
module CatapultSettingFlagBits
	VOICEMAIL_TRANSCRIPTION_DISABLED = 0
	MMS_ON_OOB_URL = 1
end

module SGXbwmsgsv2
	extend Blather::DSL

	@registration_repo = RegistrationRepo.new
	@client = SGXClient.new
	@gateway_features = [
		"http://jabber.org/protocol/disco#info",
		"http://jabber.org/protocol/address/",
		"jabber:iq:register"
	]

	def self.run
		# TODO: read/save ARGV[7] creds to local variables
		client.run
	end

	# so classes outside this module can write messages, too
	def self.write(stanza)
		client.write(stanza)
	end

	def self.before_handler(type, *guards, &block)
		client.register_handler_before(type, *guards, &block)
	end

	def self.send_media(from, to, media_url, desc=nil, subject=nil, m=nil)
		# we assume media_url is one of these (always the case so far):
		#  https://messaging.bandwidth.com/api/v2/users/[u]/media/[path]

		puts 'ORIG_URL: ' + media_url
		usr = to
		if media_url.start_with?('https://messaging.bandwidth.com/api/v2/users/')
			pth = media_url.split('/', 9)[8]
			# the caller must guarantee that 'to' is a bare JID
			media_url = ARGV[6] + WEBrick::HTTPUtils.escape(usr) + '/' + pth
			puts 'PROX_URL: ' + media_url
		end

		msg = m ? m.copy : Blather::Stanza::Message.new(to, "")
		msg.from = from
		msg.subject = subject if subject

		# provide URL in XEP-0066 (OOB) fashion
		x = Nokogiri::XML::Node.new 'x', msg.document
		x['xmlns'] = 'jabber:x:oob'

		urln = Nokogiri::XML::Node.new 'url', msg.document
		urlc = Nokogiri::XML::Text.new media_url, msg.document
		urln.add_child(urlc)
		x.add_child(urln)

		if desc
			descn = Nokogiri::XML::Node.new('desc', msg.document)
			descc = Nokogiri::XML::Text.new(desc, msg.document)
			descn.add_child(descc)
			x.add_child(descn)
		end

		msg.add_child(x)

		write(msg)
	rescue Exception => e
		panic(e)
	end

	setup ARGV[0], ARGV[1], ARGV[2], ARGV[3], nil, nil, async: true

	def self.pass_on_message(m, users_num, jid)
		# setup delivery receipt; similar to a reply
		rcpt = ReceiptMessage.new(m.from.stripped)
		rcpt.from = m.to

		# pass original message (before sending receipt)
		m.to = jid
		m.from = "#{users_num}@#{ARGV[0]}"

		puts 'XRESPONSE0: ' + m.inspect
		write_to_stream m

		# send a delivery receipt back to the sender
		# TODO: send only when requested per XEP-0184
		# TODO: pass receipts from target if supported

		# TODO: put in member/instance variable
		rcpt['id'] = SecureRandom.uuid
		rcvd = Nokogiri::XML::Node.new 'received', rcpt.document
		rcvd['xmlns'] = 'urn:xmpp:receipts'
		rcvd['id'] = m.id
		rcpt.add_child(rcvd)

		puts 'XRESPONSE1: ' + rcpt.inspect
		write_to_stream rcpt
	end

	def self.call_catapult(
		token, secret, m, pth, body=nil,
		head={}, code=[200], respond_with=:body
	)
		# pth looks like one of:
		#  "api/v2/users/#{user_id}/[endpoint_name]"

		url_prefix = ''

		# TODO: need to make a separate thing for voice.bw.c eventually
		if pth.start_with? 'api/v2/users'
			url_prefix = 'https://messaging.bandwidth.com/'
		end

		EM::HttpRequest.new(
			url_prefix + pth
		).public_send(
			m,
			head: {
				'Authorization' => [token, secret]
			}.merge(head),
			body: body
		).then { |http|
			puts "API response to send: #{http.response} with code"\
				" response.code #{http.response_header.status}"

			if code.include?(http.response_header.status)
				case respond_with
				when :body
					http.response
				when :headers
					http.response_header
				else
					http
				end
			else
				EMPromise.reject(
					BandwidthError.for(http.response_header.status, http.response)
				)
			end
		}
	end

	def self.to_catapult_possible_oob(s, num_dest, user_id, token, secret,
		usern)
		un = s.at("oob|x > oob|url", oob: "jabber:x:oob")
		unless un
			puts "MMSOOB: no url node found so process as normal"
			return to_catapult(s, nil, num_dest, user_id, token,
				secret, usern)
		end
		puts "MMSOOB: found a url node - checking if to make MMS..."

		body = s.respond_to?(:body) ? s.body : ''
		EM::HttpRequest.new(un.text, tls: { verify_peer: true }).head.then { |http|
			# If content is too large, or MIME type is not supported, place the link inside the body and do not send MMS.
			if http.response_header["CONTENT_LENGTH"].to_i > 3500000 ||
			   !MMS_MIME_TYPES.include?(http.response_header["CONTENT_TYPE"])
				unless body.include?(un.text)
					s.body = body.empty? ? un.text : "#{body}\n#{un.text}"
				end
				to_catapult(s, nil, num_dest, user_id, token, secret, usern)
			else # If size is less than ~3.5MB, strip the link from the body and attach media in the body.
				# some clients send URI in both body & <url/> so delete
				s.body = body.sub(/\s*#{Regexp.escape(un.text)}\s*$/, '')

				puts "MMSOOB: url text is '#{un.text}'"
				puts "MMSOOB: the body is '#{body.to_s.strip}'"

				puts "MMSOOB: sending MMS since found OOB & user asked"
				to_catapult(s, un.text, num_dest, user_id, token, secret, usern)
			end
		}
	end

	def self.to_catapult(s, murl, num_dest, user_id, token, secret, usern)
		body = s.respond_to?(:body) ? s.body.to_s : ''
		if murl.to_s.empty? && body.strip.empty?
			return EMPromise.reject(
				[:modify, 'policy-violation']
			)
		end

		segment_size = body.ascii_only? ? 160 : 70
		if !murl && ENV["MMS_PATH"] && body.length > segment_size*3
			file = Multibases.pack(
				'base58btc',
				Multihashes.encode(Digest::SHA256.digest(body), "sha2-256")
			).to_s
			File.open("#{ENV['MMS_PATH']}/#{file}", "w") { |fh| fh.write body }
			murl = "#{ENV['MMS_URL']}/#{file}.txt"
			body = ""
		end

		extra = {}
		extra[:media] = murl if murl

		call_catapult(
			token,
			secret,
			:post,
			"api/v2/users/#{user_id}/messages",
			JSON.dump(extra.merge(
				from: usern,
				to:   num_dest,
				text: body,
				applicationId:  ARGV[4],
				tag:
					# callbacks need id and resourcepart
					WEBrick::HTTPUtils.escape(s.id.to_s) +
					' ' +
					WEBrick::HTTPUtils.escape(
						s.from.resource.to_s
					)
			)),
			{'Content-Type' => 'application/json'},
			[201]
		).catch { |e|
			EMPromise.reject(
				[:cancel, 'internal-server-error', e.message]
			)
		}
	end

	def self.validate_num(m)
		# if sent to SGX domain use https://wiki.soprani.ca/SGX/GroupMMS
		if m.to == ARGV[0]
			an = m.children.find { |v| v.element_name == "addresses" }
			if not an
				return EMPromise.reject(
					[:cancel, 'item-not-found']
				)
			end
			puts "ADRXEP: found an addresses node - iterate addrs.."

			nums = []
			an.children.each do |e|
				num = ''
				type = ''
				e.attributes.each do |c|
					if c[0] == 'type'
						if c[1] != 'to'
							# TODO: error
						end
						type = c[1].to_s
					elsif c[0] == 'uri'
						if !c[1].to_s.start_with? 'sms:'
							# TODO: error
						end
						num = c[1].to_s[4..-1]
						# TODO: confirm num validates
						# TODO: else, error - unexpected name
					end
				end
				if num.empty? or type.empty?
					# TODO: error
				end
				nums << num
			end
			return nums
		end

		# if not sent to SGX domain, then assume destination is in 'to'
		EMPromise.resolve(m.to.node.to_s).then { |num_dest|
			if num_dest =~ /\A\+?[0-9]+(?:;.*)?\Z/
				next num_dest if num_dest[0] == '+'

				shortcode = extract_shortcode(num_dest)
				next shortcode if shortcode
			end

			if anonymous_tel?(num_dest)
				EMPromise.reject([:cancel, 'gone'])
			else
				# TODO: text re num not (yet) supportd/implmentd
				EMPromise.reject([:cancel, 'item-not-found'])
			end
		}
	end

	def self.fetch_catapult_cred_for(jid)
		@registration_repo.find(jid).then { |creds|
			if creds.length < 4
				# TODO: add text re credentials not registered
				EMPromise.reject(
					[:auth, 'registration-required']
				)
			else
				creds
			end
		}
	end

	message :error? do |m|
		# TODO: report it somewhere/somehow - eat for now so no err loop
		puts "EATERROR1: #{m.inspect}"
	end

	message :body do |m|
		EMPromise.all([
			validate_num(m),
			fetch_catapult_cred_for(m.from)
		]).then { |(num_dest, creds)|
			@registration_repo.find_jid(num_dest).then { |jid|
				[jid, num_dest] + creds
			}
		}.then { |(jid, num_dest, *creds)|
			if jid
				@registration_repo.find(jid).then { |other_user|
					[jid, num_dest] + creds + [other_user.first]
				}
			else
				[jid, num_dest] + creds + [nil]
			end
		}.then { |(jid, num_dest, *creds, other_user)|
			# if destination user is in the system pass on directly
			if other_user and not other_user.start_with? 'u-'
				pass_on_message(m, creds.last, jid)
			else
				to_catapult_possible_oob(m, num_dest, *creds)
			end
		}.catch { |e|
			if e.is_a?(Array) && (e.length == 2 || e.length == 3)
				write_to_stream m.as_error(e[1], e[0], e[2])
			else
				EMPromise.reject(e)
			end
		}
	end

	def self.user_cap_identities
		[{category: 'client', type: 'sms'}]
	end

	# TODO: must re-add stuff so can do ad-hoc commands
	def self.user_cap_features
		["urn:xmpp:receipts"]
	end

	def self.add_gateway_feature(feature)
		@gateway_features << feature
		@gateway_features.uniq!
	end

	subscription :request? do |p|
		puts "PRESENCE1: #{p.inspect}"

		# subscriptions are allowed from anyone - send reply immediately
		msg = Blather::Stanza::Presence.new
		msg.to = p.from
		msg.from = p.to
		msg.type = :subscribed

		puts 'RESPONSE5a: ' + msg.inspect
		write_to_stream msg

		# send a <presence> immediately; not automatically probed for it
		# TODO: refactor so no "presence :probe? do |p|" duplicate below
		caps = Blather::Stanza::Capabilities.new
		# TODO: user a better node URI (?)
		caps.node = 'http://catapult.sgx.soprani.ca/'
		caps.identities = user_cap_identities
		caps.features = user_cap_features

		msg = caps.c
		msg.to = p.from
		msg.from = p.to.to_s + '/sgx'

		puts 'RESPONSE5b: ' + msg.inspect
		write_to_stream msg

		# need to subscribe back so Conversations displays images inline
		msg = Blather::Stanza::Presence.new
		msg.to = p.from.to_s.split('/', 2)[0]
		msg.from = p.to.to_s.split('/', 2)[0]
		msg.type = :subscribe

		puts 'RESPONSE5c: ' + msg.inspect
		write_to_stream msg
	end

	presence :probe? do |p|
		puts 'PRESENCE2: ' + p.inspect

		caps = Blather::Stanza::Capabilities.new
		# TODO: user a better node URI (?)
		caps.node = 'http://catapult.sgx.soprani.ca/'
		caps.identities = user_cap_identities
		caps.features = user_cap_features

		msg = caps.c
		msg.to = p.from
		msg.from = p.to.to_s + '/sgx'

		puts 'RESPONSE6: ' + msg.inspect
		write_to_stream msg
	end

	iq '/iq/ns:query', ns:	'http://jabber.org/protocol/disco#info' do |i|
		# TODO: return error if i.type is :set - if it is :reply or
		#  :error it should be ignored (as the below does currently);
		#  review specification to see how to handle other type values
		if i.type != :get
			puts 'DISCO iq rcvd, of non-get type "' + i.type.to_s +
				'" for message "' + i.inspect + '"; ignoring...'
			next
		end

		# respond to capabilities request for an sgx-bwmsgsv2 number JID
		if i.to.node
			# TODO: confirm the node URL is expected using below
			#puts "XR[node]: #{xpath_result[0]['node']}"

			msg = i.reply
			msg.node = i.node
			msg.identities = user_cap_identities
			msg.features = user_cap_features

			puts 'RESPONSE7: ' + msg.inspect
			write_to_stream msg
			next
		end

		# respond to capabilities request for sgx-bwmsgsv2 itself
		msg = i.reply
		msg.node = i.node
		msg.identities = [{
			name: 'Soprani.ca Gateway to XMPP - Bandwidth API V2',
			type: 'sms', category: 'gateway'
		}]
		msg.features = @gateway_features
		write_to_stream msg
	end

	def self.check_then_register(i, *creds)
		@registration_repo
			.put(i.from, *creds)
			.catch_only(RegistrationRepo::Conflict) { |e|
				EMPromise.reject([:cancel, 'conflict', e.message])
			}.then {
				write_to_stream i.reply
			}
	end

	def self.creds_from_registration_query(i)
		if i.query.find_first("./ns:x", ns: "jabber:x:data")
			[
				i.form.field("nick")&.value,
				i.form.field("username")&.value,
				i.form.field("password")&.value,
				i.form.field("phone")&.value
			]
		else
			[i.nick, i.username, i.password, i.phone]
		end
	end

	def self.process_registration(i)
		EMPromise.resolve(nil).then {
			if i.remove?
				@registration_repo.delete(i.from).then do
					write_to_stream i.reply
					EMPromise.reject(:done)
				end
			else
				creds_from_registration_query(i)
			end
		}.then { |user_id, api_token, api_secret, phone_num|
			if phone_num && phone_num[0] == '+'
				[user_id, api_token, api_secret, phone_num]
			else
				# TODO: add text re number not (yet) supported
				EMPromise.reject([:cancel, 'item-not-found'])
			end
		}.then { |user_id, api_token, api_secret, phone_num|
			# TODO: find way to verify #{phone_num}, too
			call_catapult(
				api_token,
				api_secret,
				:get,
				"api/v2/users/#{user_id}/media"
			).then { |response|
				JSON.parse(response)
				# TODO: confirm response is array - could be empty

				puts "register got str #{response.to_s[0..999]}"

				check_then_register(
					i,
					user_id,
					api_token,
					api_secret,
					phone_num
				)
			}
		}.catch_only(BandwidthError) { |e|
			EMPromise.reject(case e.code
			when 401
				# TODO: add text re bad credentials
				[:auth, 'not-authorized']
			when 404
				# TODO: add text re number not found or disabled
				[:cancel, 'item-not-found']
			else
				[:modify, 'not-acceptable']
			end)
		}
	end

	def self.registration_form(orig, existing_number=nil)
		orig.registered = !!existing_number

		# TODO: update "User Id" x2 below (to "accountId"?), and others?
		orig.instructions = "Enter the information from your Account "\
			"page as well as the Phone Number\nin your "\
			"account you want to use (ie. '+12345678901')"\
			".\nUser Id is nick, API Token is username, "\
			"API Secret is password, Phone Number is phone"\
			".\n\nThe source code for this gateway is at "\
			"https://gitlab.com/soprani.ca/sgx-bwmsgsv2 ."\
			"\nCopyright (C) 2017-2020  Denver Gingerich "\
			"and others, licensed under AGPLv3+."
		orig.nick = ""
		orig.username = ""
		orig.password = ""
		orig.phone = existing_number.to_s

		orig.form.fields = [
			{
				required: true, type: :"text-single",
				label: 'User Id', var: 'nick'
			},
			{
				required: true, type: :"text-single",
				label: 'API Token', var: 'username'
			},
			{
				required: true, type: :"text-private",
				label: 'API Secret', var: 'password'
			},
			{
				required: true, type: :"text-single",
				label: 'Phone Number', var: 'phone',
				value: existing_number.to_s
			}
		]
		orig.form.title = 'Register for '\
			'Soprani.ca Gateway to XMPP - Bandwidth API V2'
		orig.form.instructions = "Enter the details from your Account "\
			"page as well as the Phone Number\nin your "\
			"account you want to use (ie. '+12345678901')"\
			".\n\nThe source code for this gateway is at "\
			"https://gitlab.com/soprani.ca/sgx-bwmsgsv2 ."\
			"\nCopyright (C) 2017-2020  Denver Gingerich "\
			"and others, licensed under AGPLv3+."

		orig
	end

	ibr do |i|
		puts "IQ: #{i.inspect}"

		case i.type
		when :set
			process_registration(i)
		when :get
			bare_jid = i.from.stripped
			@registration_repo.find(bare_jid).then { |creds|
				reply = registration_form(i.reply, creds.last)
				puts "RESPONSE2: #{reply.inspect}"
				write_to_stream reply
			}
		else
			# Unknown IQ, ignore for now
			EMPromise.reject(:done)
		end.catch { |e|
			if e.is_a?(Array) && (e.length == 2 || e.length == 3)
				write_to_stream i.as_error(e[1], e[0], e[2])
			elsif e != :done
				EMPromise.reject(e)
			end
		}.catch(&method(:panic))
	end

	iq type: [:get, :set] do |iq|
		write_to_stream(Blather::StanzaError.new(
			iq,
			'feature-not-implemented',
			:cancel
		))
	end
end

class ReceiptMessage < Blather::Stanza
	def self.new(to=nil)
		node = super :message
		node.to = to
		node
	end
end

class WebhookHandler < Goliath::API
	use Sentry::Rack::CaptureExceptions
	use Goliath::Rack::Params

	def response(env)
		@registration_repo = RegistrationRepo.new
		# TODO: add timestamp grab here, and MUST include ./tai version

		puts 'ENV: ' + env.reject { |k| k == 'params' }.to_s

		if params.empty?
			puts 'PARAMS empty!'
			return [200, {}, "OK"]
		end

		if env['REQUEST_URI'] != '/'
			puts 'BADREQUEST1: non-/ request "' +
				env['REQUEST_URI'] + '", method "' +
				env['REQUEST_METHOD'] + '"'
			return [200, {}, "OK"]
		end

		if env['REQUEST_METHOD'] != 'POST'
			puts 'BADREQUEST2: non-POST request; URI: "' +
				env['REQUEST_URI'] + '", method "' +
				env['REQUEST_METHOD'] + '"'
			return [200, {}, "OK"]
		end

		# TODO: process each message in list, not just first one
		jparams = params.dig('_json', 0, 'message')
		type = params.dig('_json', 0, 'type')

		return [400, {}, "Missing params\n"] unless jparams && type

		users_num, others_num = if jparams['direction'] == 'in'
			[jparams['owner'], jparams['from']]
		elsif jparams['direction'] == 'out'
			[jparams['from'], jparams['owner']]
		else
			puts "big prob: '#{jparams['direction']}'"
			return [400, {}, "OK"]
		end

		return [400, {}, "Missing params\n"] unless users_num && others_num
		return [400, {}, "Missing params\n"] unless jparams['to'].is_a?(Array)

		puts "BODY - messageId: #{jparams['id']}" \
			", eventType: #{type}" \
			", time: #{jparams['time']}" \
			", direction: #{jparams['direction']}" \
			", deliveryState: #{jparams['deliveryState'] || 'NONE'}" \
			", errorCode: #{jparams['errorCode'] || 'NONE'}" \
			", description: #{jparams['description'] || 'NONE'}" \
			", tag: #{jparams['tag'] || 'NONE'}" \
			", media: #{jparams['media'] || 'NONE'}"

		if others_num[0] != '+'
			# TODO: check that others_num actually a shortcode first
			others_num +=
				';phone-context=ca-us.phone-context.soprani.ca'
		end

		bare_jid = @registration_repo.find_jid(users_num).sync

		if !bare_jid
			puts "jid_key for (#{users_num}) DNE; BW API misconfigured?"

			return [403, {}, "Customer not found\n"]
		end

		msg = nil
		case jparams['direction']
		when 'in'
			text = ''
			case type
			when 'sms'
				text = jparams['text']
			when 'mms'
				has_media = false

				if jparams['text'].empty?
					if not has_media
						text = '[suspected group msg '\
							'with no text (odd)]'
					end
				else
					text = if has_media
						# TODO: write/use a caption XEP
						jparams['text']
					else
						'[suspected group msg '\
						'(recipient list not '\
						'available) with '\
						'following text] ' +
						jparams['text']
					end
				end

				# ie. if text param non-empty or had no media
				if not text.empty?
					msg = Blather::Stanza::Message.new(
						bare_jid, text)
					msg.from = others_num + '@' + ARGV[0]
					SGXbwmsgsv2.write(msg)
				end

				return [200, {}, "OK"]
			when 'message-received'
				# TODO: handle group chat, and fix above
				text = jparams['text']

				if jparams['to'].length > 1
					msg = Blather::Stanza::Message.new(
						Blather::JID.new(bare_jid).domain,
						text
					)

					addrs = Nokogiri::XML::Node.new(
						'addresses', msg.document)
					addrs['xmlns'] = 'http://jabber.org/' \
						'protocol/address'

					addr1 = Nokogiri::XML::Node.new(
						'address', msg.document)
					addr1['type'] = 'to'
					addr1['jid'] = bare_jid
					addrs.add_child(addr1)

					jparams['to'].each do |receiver|
						if receiver == users_num
							# already there in addr1
							next
						end

						addrn = Nokogiri::XML::Node.new(
							'address', msg.document)
						addrn['type'] = 'to'
						addrn['uri'] = "sms:#{receiver}"
						addrn['delivered'] = 'true'
						addrs.add_child(addrn)
					end

					msg.add_child(addrs)

					# TODO: delete
					puts "RESPONSE9: #{msg.inspect}"
				end

				Array(jparams['media']).each do |media_url|
					unless media_url.end_with?(
						'.smil', '.txt', '.xml'
					)
						has_media = true
						SGXbwmsgsv2.send_media(
							others_num + '@' +
							ARGV[0],
							bare_jid, media_url,
							nil, nil, msg
						)
					end
				end
			else
				text = "unknown type (#{type})"\
					" with text: " + jparams['text']

				# TODO: log/notify of this properly
				puts text
			end

			if not msg
				msg = Blather::Stanza::Message.new(bare_jid, text)
			end
		else # per prior switch, this is:  jparams['direction'] == 'out'
			tag_parts = jparams['tag'].split(/ /, 2)
			id = WEBrick::HTTPUtils.unescape(tag_parts[0])
			resourcepart = WEBrick::HTTPUtils.unescape(tag_parts[1])

			# TODO: remove this hack
			if jparams['to'].length > 1
				puts "WARN! group no rcpt: #{users_num}"
				return [200, {}, "OK"]
			end

			case type
			when 'message-failed'
				# create a bare message like the one user sent
				msg = Blather::Stanza::Message.new(
					others_num + '@' + ARGV[0])
				msg.from = bare_jid + '/' + resourcepart
				msg['id'] = id

				# TODO: add 'errorCode' and/or 'description' val
				# create an error reply to the bare message
				msg = msg.as_error(
					'recipient-unavailable',
					:wait,
					jparams['description']
				)

				# TODO: make prettier: this should be done above
				others_num = params['_json'][0]['to']
			when 'message-delivered'

				msg = ReceiptMessage.new(bare_jid)

				# TODO: put in member/instance variable
				msg['id'] = SecureRandom.uuid

				# TODO: send only when requested per XEP-0184
				rcvd = Nokogiri::XML::Node.new(
					'received',
					msg.document
				)
				rcvd['xmlns'] = 'urn:xmpp:receipts'
				rcvd['id'] = id
				msg.add_child(rcvd)

				# TODO: make prettier: this should be done above
				others_num = params['_json'][0]['to']
			else
				# TODO: notify somehow of unknown state receivd?
				puts "message with id #{id} has "\
					"other type #{type}"
				return [200, {}, "OK"]
			end

			puts "RESPONSE4: #{msg.inspect}"
		end

		msg.from = others_num + '@' + ARGV[0]
		SGXbwmsgsv2.write(msg)

		[200, {}, "OK"]
	rescue Exception => e
		Sentry.capture_exception(e)
		puts 'Shutting down gateway due to exception 013: ' + e.message
		SGXbwmsgsv2.shutdown
		puts 'Gateway has terminated.'
		EM.stop
	end
end

at_exit do
	$stdout.sync = true

	puts "Soprani.ca/SMS Gateway for XMPP - Bandwidth API V2\n"\
		"==>> last commit of this version is " + `git rev-parse HEAD` + "\n"

	if ARGV.size != 7
		puts "Usage: sgx-bwmsgsv2.rb <component_jid> "\
			"<component_password> <server_hostname> "\
			"<server_port> <application_id> "\
			"<http_listen_port> <mms_proxy_prefix_url>"
		exit 0
	end

	t = Time.now
	puts "LOG %d.%09d: starting...\n\n" % [t.to_i, t.nsec]

	EM.run do
		REDIS = EM::Hiredis.connect

		SGXbwmsgsv2.run

		# required when using Prosody otherwise disconnects on 6-hour inactivity
		EM.add_periodic_timer(3600) do
			msg = Blather::Stanza::Iq::Ping.new(:get, 'localhost')
			msg.from = ARGV[0]
			SGXbwmsgsv2.write(msg)
		end

		server = Goliath::Server.new('0.0.0.0', ARGV[5].to_i)
		server.api = WebhookHandler.new
		server.app = Goliath::Rack::Builder.build(server.api.class, server.api)
		server.logger = Log4r::Logger.new('goliath')
		server.logger.add(Log4r::StdoutOutputter.new('console'))
		server.logger.level = Log4r::INFO
		server.start do
			["INT", "TERM"].each do |sig|
				trap(sig) do
					EM.defer do
						puts 'Shutting down gateway...'
						SGXbwmsgsv2.shutdown

						puts 'Gateway has terminated.'
						EM.stop
					end
				end
			end
		end
	end
end unless ENV['ENV'] == 'test'
